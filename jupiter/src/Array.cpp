#include "stdafx.h"

#include <jupiter/Array.hxx>

namespace jupiter {
    Array::Array ( size_t length )
        : length_ ( length ), data_ ( null ) {
        data_ = new jupiter::Object[length];
    }

    Array* const Array::createInstance ( size_t length ) {
        return new Array ( length );
    }
}
