﻿#include "stdafx.h"

#include <jupiter/io/TextWriter.hxx>
#include <jupiter/text/Encoding.hxx>


namespace jupiter {
    namespace io {


        TextWriter::TextWriter()
            : format_provider_(null) {
        }

        TextWriter::TextWriter(jupiter::IFormatProvider* const& format_provider)
            : format_provider_(format_provider) { 
        }

        TextWriter::~TextWriter() {
        }

        jupiter::IFormatProvider* const TextWriter::getFormatProvider() const {
            if ( format_provider_ == null ) {
                return Thread::getCurrentThread()->getCurrentCulture();
            }
            return format_provider_;
        }

        void TextWriter::close() {}

        void TextWriter::flush() {}

        void TextWriter::write(const jupiter::Object& value) {
        }

        void TextWriter::write(const jupiter::String& value) {
        }

        void TextWriter::write(const jupiter::String& format, const jupiter::array<jupiter::Object*>& args) {
        }


    }
}
