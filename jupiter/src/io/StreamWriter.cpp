#include "stdafx.h"

#include <jupiter/text/Encoding.hxx>
#include <jupiter/io/Stream.hxx>
#include <jupiter/io/StreamWriter.hxx>
#include <jupiter/io/FileMode.hxx>
#include <jupiter/io/FileAccess.hxx>
#include <jupiter/io/FileShare.hxx>


namespace jupiter {
    namespace io {


        static jupiter::text::Encoding        default_encoding__;

        jupiter::io::Stream* createFile(jupiter::String* const& path, bool append, bool check_host) {
            auto    mode          = append
                ? jupiter::io::FileMode::Append
                : jupiter::io::FileMode::Create;
            auto    result_stream = new FileStream( path,
                                                    mode,
                                                    jupiter::io::FileAccess::Write,
                                                    jupiter::io::FileShare::Read );

            return result_stream;
        }

#define     DEFAULT_BUFFER_SIZE           (1024)
#define     MIN_BUFFER_SIZE                (128)

        StreamWriter::StreamWriter( jupiter::io::Stream* const& stream )
            : _Super( null ), stream_( null ), buffer_( null ), buffer_size_( 0 ), leave_open_( false ) {
            // TODO: 引数のチェック
            init( stream, &default_encoding__, DEFAULT_BUFFER_SIZE, false );
        }

        StreamWriter::~StreamWriter() {
            dispose();
        }

        void StreamWriter::close() {
            dispose( true );
        }

        void StreamWriter::flush() {
            stream_->flush();
        }

        void StreamWriter::write( const jupiter::Object& value ) {
        }

        void StreamWriter::write( const jupiter::String& value ) {
        }

        void StreamWriter::write( const jupiter::String& format, const jupiter::array<jupiter::Object*>& args ) {
        }

        void StreamWriter::dispose(bool disposing) {
#ifdef COMPILER_IS_MSVC
            __try {
                if ( this->stream_ != null ) {

                    if ( disposing || this->getLeaveOpen() && TYPE_P(this->stream_, ConsoleStream)  ) {
                        this->flush( true, true );
                    }
                    
                }
            } __finally {
                if ( this->getLeaveOpen() && this->stream_ != null ) {
                    __try {
                        if ( disposing ) {
                            this->stream_->close();
                        }
                    } __finally {
                        this->stream_->release();
                        this->stream_      = null;

                        delete this->byte_buffer_;
                        this->byte_buffer_ = null;

                        delete this->char_buffer_;
                        this->char_buffer_ = null;

                        this->encoding_    = null;
                        this->encoder_->release();
                        this->encoder_     = null;
                        this->char_length  = null;
                        _Super::dispose( diposing );
                    }
                }
            }
#endif  /* def COMPILER_IS_MSVC */
        }

        void StreamWriter::init(jupiter::io::Stream* const& a_stream, jupiter::text::Encoding* const& an_encoding, int buffer_size, bool should_leave_open) {
            this->stream_   = a_stream;
            this->encoding_ = an_encoding;
            this->encoder_  = this->encoding_->getEncoder();

            if ( buffer_size < MIN_BUFFER_SIZE ) {
                buffer_size = MIN_BUFFER_SIZE;
            }

            this->char_buffer_ = new jupiter::array<char>( buffer_size );
            this->byte_buffer_ = new jupiter::array<byte>( this->encoding_->getMaxByteCount( buffer_size ) );

            this->char_length_ = buffer_size;

            if ( this->stream_->canSeek() && this->stream_->getPosition() > 0 ) {
                this->have_written_preamble_ = true;
            }
            this->closable_ = !should_leave_open;
        }


    }
}
