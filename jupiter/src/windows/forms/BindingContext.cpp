﻿#include "stdafx.h"

#include <jupiter/jupiter.hxx>
#include <jupiter/windows/forms/BindingContext.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {


            BindingContext::BindingContext() {
            }

            BindingContext::~BindingContext() {}

            BindingManagerBase* const BindingContext::at ( jupiter::Object* const& data_source ) {}

            BindingManagerBase* const BindingContext::at ( jupiter::Object* const& data_source, jupiter::String* const& data_member ) {}

        }
    }
}