﻿#include "stdafx.h"

#include <jupiter/jupiter.hxx>
#include <jupiter/windows/forms/Form.hxx>
#include <jupiter/windows/forms/Application.hxx>
#include <jupiter/windows/forms/ApplicationContext.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {
            
            
            void Application::enableVisualStyles() {
            }

            void Application::setCompatibleTextRenderingDefault ( bool default_value ) {
            }

            void Application::run ( Form* const& main_form ) {
                ApplicationContext* context = new ApplicationContext ( main_form );

                run ( context );
            }

            void Application::run ( ApplicationContext* const& context ) {
            }


        }
    }
}
