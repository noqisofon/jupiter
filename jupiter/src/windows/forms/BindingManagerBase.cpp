﻿#include "stdafx.h"

#include <jupiter/jupiter.hxx>
#include <jupiter/windows/forms/BindingManagerBase.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {


            BindingManagerBase::BindingManagerBase()
                : bindings_ ( null ), is_binding_suspended_ ( false ) {
            }

            void BindingManagerBase::pullData() {
                if ( bindings_ == null ) {

                    return ;
                }
            }

            void BindingManagerBase::pushData() {
                if ( bindings_ == null ) {

                    return ;
                }
            }


        }
    }
}
