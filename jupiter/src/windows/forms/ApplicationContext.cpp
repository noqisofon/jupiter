﻿#include "stdafx.h"

#include <jupiter/jupiter.hxx>
#include <jupiter/windows/forms/ApplicationContext.hxx>
#include <jupiter/windows/forms/Form.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {


            ApplicationContext::ApplicationContext()
                : main_form_ ( null ), tag_ ( null ) {
            }

            ApplicationContext::ApplicationContext ( Form* const main_form )
                : main_form_ ( main_form ), tag_ ( null ) {
            }

            ApplicationContext::~ApplicationContext() {
                if ( main_form_ != null ) {
                    main_form_->release();
                }

                if ( tag_ != null ) {
                    tag_->release();
                }
            }

            void ApplicationContext::dispose() {
            }

            void ApplicationContext::dispose ( bool disposing ) {
            }

            void ApplicationContext::exitThread() {
            }


        }
    }
}
