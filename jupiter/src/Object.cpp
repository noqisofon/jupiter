﻿#include "stdafx.h"

#include <jupiter/Object.hxx>
#include <jupiter/String.hxx>

namespace jupiter {


    Object::Object() {
    }

    Object::~Object() {
    }

    bool Object::equals ( const Object* const& other ) const {
        return this == other;
    }

    String* const Object::toString() const {
        return getType()->toString();
    }

    void Object::release() {
    }

    void Object::retain() {
    }


}
