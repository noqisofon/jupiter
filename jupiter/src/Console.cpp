﻿#include "stdafx.h"

#include <jupiter/Console.hxx>


class initializer {
 public:
    initializer() {
        jupiter::Console::__initialize();
    }

    ~initializer() {
        jupiter::Console::__finalize();
    }
};

initializer console_initializer;


namespace jupiter {


    void Console::write(const jupiter::String* const& value) {
        out__.write( value );
    }

    void Console::write(const jupiter::String* const& format, const jupiter::array<jupiter::Object *>& args) {
        out__.write( format, args );
    }

    void Console::writeLine(const jupiter::String* const& value) {
        out__.writeLine( value );
    }

    void Console::writeLine(const jupiter::String* const& format, const jupiter::array<jupiter::Object *>& args) {
        out__.writeLine( format, args );
    }


}
