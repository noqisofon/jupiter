#pragma once

#include <jupiter/jupiter.hxx>

namespace jupiter {


    interface IDisposable {
        JUPITER_BECOME_INTERFACE ( IDisposable );

        virtual void dispose() __pure;
    };


}
