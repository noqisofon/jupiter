﻿#pragma once

#include <jupiter/Object.hxx>
#include <jupiter/Type.hxx>
#include <jupiter/runtime/remoting/ObjRef.hxx>


namespace jupiter {


    /*!
     *
     */
    abstract class MarshalByRefObject : public Object {
     protected:
        /*!
         *
         */
        MarshalByRefObject();

     public:
        /*!
         *
         */
        virtual ~MarshalByRefObject();

     public:
        /*!
         * リモートオブジェクトと通信するために使用するプロキシを生成するために必要な全ての関連情報を火口脳しているオブジェクトを生成して返します。
         */
        virtual jupiter::runtime::remoting::ObjRef* const createObjRef(jupiter::Type* const& requested_type) const;

        /*!
         * このインスタンスの有効期間ポリシーを制御する現在の有効期間サービスオブジェクトを初期化します。
         */
        virtual jupiter::Object* const initializeLifetimeService();

        /*!
         * このインスタンスの有効期間ポリシーを制御する現在の有効期間サービスオブジェクトを取得します。
         */
        virtual jupiter::Object* const getLifetimeService() const;
    };


}
