﻿#pragma once

#include <jupiter/top.h>
#include <jupiter/jupiter.hxx>
#include <jupiter/primitive_array.h>
#include <jupiter/Object.hxx>
#include <jupiter/String.hxx>
#include <jupiter/io/TextWriter.hxx>


namespace jupiter {


    abstract sealed class Console {
        JUPITER_BECOME_STATIC_CLASS(Console);

     public:
        static void write(const jupiter::String* const& value);
        static void write(const jupiter::String* const& format, const jupiter::array<jupiter::Object *>& args);
        static void writeLine(const jupiter::String* const& value);
        static void writeLine(const jupiter::String* const& format, const jupiter::array<jupiter::Object *>& args);

        static void __initialize();
        static void __finalize();

     private:
        static jupiter::io::TextWriter* out__;
    };
    

}
