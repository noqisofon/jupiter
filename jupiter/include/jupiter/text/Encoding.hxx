#pragma once

#include <jupiter/jupiter.hxx>


namespace jupiter {
    namespace text {


        abstract class Encoding : public jupiter::Object
                                , public jupiter::ICloneable {
         protected:
            Encoding();
        };


    }
}
