#pragma once

#include <jupiter/jupiter.hxx>
#include <jupiter/Type.hxx>


namespace jupiter {


    interface IFormatProvider {
        JUPITER_BECOME_INTERFACE( IFormatProvider );

        virtual jupiter::Object* const getFormat(const jupiter::Type* const& format_type) __pure;
    };


}
