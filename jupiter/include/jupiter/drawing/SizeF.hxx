﻿#pragma once

namespace jupiter {
    namespace drawing {
        class SizeF {
          public:
            /*!
            *
            */
            SizeF ( float width, float height );

          public:
            float getWidth()               const {
                return width_;
            }
            void  setWidth ( float value ) {
                width_ = value;
            }

            float getHeight()              const {
                return height_;
            }
            void  setHeight ( float value ) {
                height_ = value;
            }

            bool    isEmpty() const {
                return width_ == 0 && height_ == 0;
            }

          private:
            float width_;
            float height_;
        };
    }
}
