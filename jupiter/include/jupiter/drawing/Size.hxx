﻿#pragma once

#include <stdint.h>

namespace jupiter {
    namespace drawing {
        class Size {
        public:
          /*!
           *
           */
            Size( int32_t width, int32_t height );

        public:
            int32_t getWidth()               const {
                return width_;
            }
            void    setWidth( int32_t value ) {
                width_ = value;
            }

            int32_t getHeight()              const {
                return height_;
            }
            void    setHeight( int32_t value ) {
                height_ = value;
            }

            bool    isEmpty() const {
                return width_ == 0 && height_ == 0;
            }

        private:
            int32_t width_;
            int32_t height_;
        };
    }
}
