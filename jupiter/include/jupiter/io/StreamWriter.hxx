#pragma once

#include <jupiter/jupiter.hxx>
#include <jupiter/io/TextWriter.hxx>


namespace jupiter {
    namespace io {


        class Stream;


        abstract class StreamWriter : public TextWriter {
            typedef TextWriter    _Super;
        public:
            StreamWriter( jupiter::io::Stream* const& stream );

            virtual ~StreamWriter();

        public:
            /*!
            *
            */
            virtual void close();

            /*!
            *
            */
            virtual void flush();

            /*!
            *
            */
            virtual void write( const jupiter::Object& value );

            /*!
            *
            */
            virtual void write( const jupiter::String& value );

            /*!
            *
            */
            virtual void write( const jupiter::String& format, const jupiter::array<jupiter::Object*>& args );

         private:
            bool getLeaveOpen() const { return !closable_; }
            
            void init(jupiter::io::Stream* const& a_stream, jupiter::text::Encoding* const& an_encoding, int buffer_size, bool should_leave_open);

         private:
            jupiter::io::Stream*     stream_;
            jupiter::text::Encoding* encoding_;
            jupiter::text::Encoder*  encoder_;
            jupiter::array<char>*    char_buffer_;
            size_t                   char_position_;
            size_t                   char_length_;
            jupiter::array<byte>*    byte_char_;
            bool                     auto_flush_;
            bool                     have_written_preamble_;
            bool                     closable_;
        };


    }
}
