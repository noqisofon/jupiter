#pragma once

#include <jupiter/jupiter.hxx>


namespace jupiter {
    namespace io {


        enum class FileMode : int {
            Append,
            Create,
            CreateNew,
            Open,
            OpenOrCreate,
            Truncate
        };


    }
}
