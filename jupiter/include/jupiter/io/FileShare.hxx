#pragma once

#include <jupiter/jupiter.hxx>


namespace jupiter {
    namespace io {


        enum class FileShare : int {
            None,
            Inheritable,
            Delete,
            Read,
            ReadWrite,
            Write
        };


    }
}
