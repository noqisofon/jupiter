#pragma once

#include <jupiter/jupiter.hxx>


namespace jupiter {
    namespace io {


        enum class FileAccess : int {
            Read,
            ReadWrite,
            Write
        };


    }
}
