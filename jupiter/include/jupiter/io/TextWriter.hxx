#pragma once

#include <jupiter/jupiter.hxx>
#include <jupiter/MarshalByRefObject.hxx>
#include <jupiter/IFormatProvider.hxx>
#include <jupiter/text/Encoding.hxx>


namespace jupiter {
    namespace io {


        abstract class TextWriter : public MarshalByRefObject {
         protected:
            /*!
             *
             */
            TextWriter();
            /*!
             *
             */
            TextWriter(jupiter::IFormatProvider* const& format_provider);

         public:
            /*!
             *
             */
            virtual  ~TextWriter();

         public:
            /*!
             *
             */
            virtual jupiter::text::Encoding* const  getEncoding()       const __pure;

            /*!
             *
             */
            virtual jupiter::IFormatProvider* const getFormatProvider() const;

         public:
            /*!
             *
             */
            virtual void close();

            /*!
             *
             */
            virtual void flush();

            /*!
             *
             */
            virtual void write(const jupiter::Object& value);

            /*!
             *
             */
            virtual void write(const jupiter::String& value);

            /*!
             *
             */
            virtual void write(const jupiter::String& format, const jupiter::array<jupiter::Object*>& args);

         private:
            jupiter::text::Encoding*    encoding_;
            jupiter::IFormatProvider*   format_provider_;
        };


    }
}
