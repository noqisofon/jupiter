﻿#pragma once

#include <jupiter/Object.hxx>


namespace jupiter {


    abstract class Delegate : public jupiter::Object {
    public:
        Delegate( jupiter::Object* const& target ) : target_( target ) {}

    public:
        jupiter::Object* const getTarget() const { return target_; }

    private:
        jupiter::Object*        target_;
    };


}
