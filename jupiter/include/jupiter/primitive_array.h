﻿#pragma once

#include <jupiter/core.h>


namespace jupiter {


    template <class _Type>
    class _array_base {
    public:
        _array_base() : first_( 0 ), size_( 0 ) {}
        _array_base( size_t __n ) : first_( 0 ), size_( 0 ) {
            _allocate( __n );
        }

        ~_array_base() {
            _deallocate();
        }

    public:
        void _allocate( size_t __n ) {
            if ( __n != 0 ) {
                first_ = STATIC_CAST( _Type *, jupiter::allocate( __n * sizeof( _type ) ) );
                size_ = __n;
            } else {
                first_ = 0;
                size_ = 0;
            }
        }

        void _deallocate() {
            jupiter::dealloc( first_ );
            first_ = 0;
            size_ = 0;
        }

    protected:
        _Type*  first_;
        size_t  size_;
    };


    template <class _Type>
    class array : private _array_base<_Type> {
        typedef    _array_base<_Type>    _Super;

    public:
        typedef    _Type    value_type;
        typedef    _Type*   pointer_type;

    public:
        array() : _Super() {}
        explicit array( size_t __n ) : _Super( __n ) {
            jupiter::fill_n( this->first_, this->size_, JUPITER_DEFAULT_VALUE( value_type ) );
        }
        array( const value_type& __x, size_t __n ) : _Super( __n ) {
            jupiter::fill_n( this->first_, this->size_, __x );
        }
        array( const pointer_type __p, size_t __n ) : _Super( __n ) {
            jupiter::copy( __p, __p + __n, this->first_ );
        }
        array( const array<_Type>& __x ) : _Super( __x.size_ ) {
            jupiter::copy( __x.first_, __x.first_ + __x.size_, this->first_ );
        }

        ~array() {
            jupiter::destroy_range( this->first_, this->first_ + this->size_ );
        }

    public:
        array<_Type>& operator = ( const array<_Type>& __other ) {
            if ( this != &__other ) {
                copy( __other.first_, __other.first_ + __other.size_, this->first_ );
            }
            return *this;
        }

        value_type operator[]( size_t __n ) const {
            return this->first_[__n];
        }

        value_type& operator[]( size_t __n ) {
            return this->first_[__n];
        }

        size_t length() const { return this->size_; }
    };


}
