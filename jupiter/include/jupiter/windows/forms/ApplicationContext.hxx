﻿#pragma once

#include <jupiter/Object.hxx>
#include <jupiter/IDisposable.hxx>
#include <jupiter/windows/forms/Form.hxx>

namespace jupiter {
    namespace windows {
        namespace forms {
            /*!
             *
             */
            class ApplicationContext : public jupiter::Object, public IDisposable {
                typedef jupiter::Object _Base;

              public:
                /*!
                 *
                 */
                ApplicationContext();
                /*!
                 *
                 */
                ApplicationContext ( Form* const main_form );

                /*!
                 *
                 */
                virtual ~ApplicationContext();

              public:
                /*!
                 *
                 */
                Form* const getMainForm()                   const {
                    return main_form_;
                }
                /*!
                 *
                 */
                void        setMainForm ( Form* const& value ) {
                    main_form_ = value;
                }

              public:
                /*!
                 *
                 */
                virtual void dispose();

                /*!
                 *
                 */
                void exitThread();

              protected:
                void dispose ( bool disposing );

              private:
                Form*               main_form_;
                jupiter::Object*    tag_;
            };
        }
    }
}
