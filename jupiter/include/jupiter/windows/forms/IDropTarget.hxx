﻿#pragma once

#include <jupiter/jupiter.hxx>

namespace jupiter {
    namespace windows {
        namespace forms {
            interface IDropTarget {
                JUPITER_BECOME_INTERFACE ( IDropTarget );

                virtual void onDragDrop() __pure;
            };
        }
    }
}
