﻿#pragma once

#include <jupiter/jupiter.hxx>
#include <jupiter/MarshalByRefObject.hxx>
#include <jupiter/collections/ICollection.hxx>
#include <jupiter/collections/IEnumerable.hxx>
#include <jupiter/collections/List.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {


            template <class _Type>
            class BaseCollection : public MarshalByRefObject
                                 , public ICollection<_Type>
                                 , public IEnumerable<_Type> {
             public:
                BaseCollection() : list_(0) {
                    list_ = new jupiter::collections::List<_Type>();
                }

             public:
                /*!
                 *
                 */
                virtual size_t getCount() const { return list_->getCount(); }

                /*!
                 *
                 */
                virtual bool isReadOnly() const { return list_->isReadOnly(); }

             protected:
                /*!
                 *
                 */
                virtual jupiter::collections::ArrayList<_Type>* const getList() const;

             public:

             private:
                jupiter::collections::List<_Type>* list_;
            };


        }
    }
}
