﻿#pragma once

#include <jupiter/MarshalByRefObject.hxx>
#include <jupiter/component_model/IComponent.hxx>
#include <jupiter/component_model/ISynhronizeInvoke.hxx>
#include <jupiter/windows/forms/IBindableComponent.hxx>
#include <jupiter/windows/forms/IDropTarget.hxx>
#include <jupiter/windows/forms/IWin32Window.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {


            class Control
                : public jupiter::MarshalByRefObject
                , public IDropTarget
                , public IWin32Window
                , public IBindableComponent
                , public jupiter::component_model::IComponent
                , public jupiter::component_model::ISynhronizeInvoke {
            };


        }
    }
}
