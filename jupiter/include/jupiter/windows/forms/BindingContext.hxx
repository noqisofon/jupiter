#pragma once

#include <jupiter/jupiter.hxx>
#include <jupiter/windows/forms/BindingManagerBase.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {


            class BindingContext : public jupiter::Object {
              public:
                /*!
                 *
                 */
                BindingContext();

                /*!
                 *
                 */
                virtual ~BindingContext();

              public:
                /*!
                 *
                 */
                virtual bool isReadOnly() const {
                    return false;
                }

                /*!
                 *
                 */
                virtual BindingManagerBase* const at ( jupiter::Object* const& data_source );

                /*!
                 *
                 */
                virtual BindingManagerBase* const at ( jupiter::Object* const& data_source, jupiter::String* const& data_member );
            };


        }
    }
}
