﻿#pragma once

#include <jupiter/windows/forms/ScrollableControl.hxx>

namespace jupiter {
    namespace windows {
        namespace forms {
            class ContainerControl : public ScrollableControl {};
        }
    }
}
