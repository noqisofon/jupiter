#pragma once

#include <jupiter/jupiter.hxx>

namespace jupiter {
    namespace windows {
        namespace forms {
            interface IWin32Window {
                JUPITER_BECOME_INTERFACE ( IWin32Window );

                virtual IntPtr getHandle() const __pure;
            };
        }
    }
}
