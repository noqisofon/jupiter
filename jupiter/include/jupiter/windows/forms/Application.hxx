﻿#pragma once

#include <jupiter/windows/forms/ApplicationContext.hxx>

namespace jupiter {
    namespace windows {
        namespace forms {
            class Application {
              public:
                /*!
                 *
                 */
                static void enableVisualStyles();

                /*!
                 *
                 */
                static void setCompatibleTextRenderingDefault ( bool default_value );

                /*!
                 *
                 */
                static void run ( Form* const& main_form );

                /*!
                *
                */
                static void run ( ApplicationContext* const& context );
            };
        }
    }
}
