#pragma once

namespace jupiter {
    namespace windows {
        namespace forms {
            enum class AutoScaleMode : int {
                Dpi,
                Font,
                Inherit,
                None
            };
        }
    }
}
