#pragma once

#include <jupiter/jupiter.hxx>

#include <jupiter/IDisposable.hxx>
#include <jupiter/component_model/ISite.hxx>
#include <jupiter/windows/forms/BindingContext.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {


            /*!
             *
             */
            interface IBindableComponent {
                JUPITER_BECOME_INTERFACE ( IBindableComponent );

                virtual BindingContext* const            getBindingContext() const __pure;
                virtual void                             setBindingContext ( BindingContext* const& value ) __pure;

                virtual ControlBindingsCollection* const getDataBindings() const __pure;
            };


        }
    }
}
