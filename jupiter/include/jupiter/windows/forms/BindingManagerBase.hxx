﻿#pragma once

#include <jupiter/jupiter.hxx>
#include <jupiter/windows/forms/BindingsCollection.hxx>


namespace jupiter {
    namespace windows {
        namespace forms {


            /*!
             *
             */
            abstract class BindingManagerBase : public jupiter::Object {
              protected:
                BindingManagerBase();

              public:
                virtual BindingsCollection* const getBindings()         const {
                    return bindings_;
                }

                virtual int                       getCount()            const __pure;

                virtual jupiter::Object*          getCurrent()          const __pure;

                virtual bool                      isBindingSuspended()  const {
                    return is_binding_suspended_;
                }

                virtual int                       getPosition()         const __pure;
                virtual void                      setPosition ( int value )      __pure;

              protected:
                virtual void                      changeBindingSuspended ( bool value ) {
                    is_binding_suspended_ = value;
                }

              public:
                /*!
                 * 派生クラスでオーバーライドされると、元になるリストに新しい項目を追加します。
                 */
                virtual void addNew() __pure;

                /*!
                 * 派生クラスでオーバーライドされると、現在の編集をキャンセルします。
                 */
                virtual void cancelCurrentEdit() __pure;

                /*!
                 * 派生クラスでオーバーライドされると、現在の編集を終了します。
                 */
                virtual void endCurrentEdit() __pure;

                /*!
                 * 派生クラスでオーバーライドされると、元になるリストから指定されたインデックスに対応する行を削除します。
                 */
                virtual void removeAt ( int index ) __pure;

                /*!
                 * 派生クラスでオーバーライドされると、データ連結を再開します。
                 */
                virtual void resumeBinding() __pure;

                /*!
                 * 派生クラスでオーバーライドされると、データ連結を中断します。
                 */
                virtual void suspendBinding() __pure;

              protected:
                /*!
                 * データバインドコントロールからデータソースにデータをプルします。
                 */
                virtual void pullData();

                /*!
                 * データバインドコントロールからデータソースにデータをプッシュします。
                 */
                virtual void pushData();

              protected:
                BindingsCollection*  bindings_;

              private:
                bool is_binding_suspended_;
            };


        }
    }
}
