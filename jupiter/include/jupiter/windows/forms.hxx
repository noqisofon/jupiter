#pragma once

namespace jupiter {
    namespace windows {
        namespace forms {}
    }
}

#include <jupiter/windows/forms/Application.hxx>
#include <jupiter/windows/forms/ApplicationContext.hxx>
#include <jupiter/windows/forms/AutoScaleMode.hxx>
#include <jupiter/windows/forms/Form.hxx>
