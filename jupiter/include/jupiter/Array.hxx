﻿#pragma once

#include <jupiter/Object.hxx>


namespace jupiter {


    class Array : public jupiter::Object {
      public:
        Array ( size_t length );

      public:
        static Array* const createInstance ( size_t length );

      private:
        size_t              length_;
        jupiter::Object*    data_;
    };


}
