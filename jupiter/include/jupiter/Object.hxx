﻿#pragma once


namespace jupiter {


    class String;
    class Type;


    class  Object {
    public:
        Object();

        virtual ~Object();

    public:
        virtual bool equals( const Object& other ) const {
            return equals( &other );
        }
        virtual bool equals( const Object* const& other ) const;

        virtual String* const toString() const;

        virtual Type* const getType() const;

        void release();
        void retain();
    };


}
