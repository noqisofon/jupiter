﻿#pragma once

#include <stddef.h>
#include <stdint.h>

#define     interface       struct
#define     abstract
#define     sealed

#define     JUPITER_BECOME_INTERFACE(_klass_name_)  \
    virtual ~ ## _klass_name_() {}

#define     JUPITER_BECOME_STATIC_CLASS(_Klass_)    \
    private:                                        \
    _Klass_() {}                                    \
    ~ ## _Klass_() {}

#define     __pure          = 0

#define     null            __nullptr

#ifdef JUPITER_EXPORTS
#   define JUPITER_API      __declspec(dllexport)
#else
#   define JUPITER_API      __declspec(dllimport)
#endif

typedef              int    IntPtr;
typedef     unsigned int    UIntPtr;

typedef     unsigned char   ubyte;

#define  __C_STYLE_CAST(_type_, _expr_)    ((_type_)(_expr_))

#ifdef JUPITER_WANT_NEW_STYLE_CASTS
#   define   __CONST_CAST(_type_, _expr_)                const_cast<_type_>(_expr_)
#   define   __STATIC_CAST(_type_, _expr_)              static_cast<_type_>(_expr_)
#   define   __REINTERPRET_CAST(_type_, _expr_)    reinterpret_cast<_type_>(_expr_)
#   define   __DYNAMIC_CAST(_type_, _expr_)            dynamic_cast<_type_>(_expr_)
#else
#   define   __CONST_CAST(_type_, _expr_)            __C_STYLE_CAST(_type_, _expr_)
#   define   __STATIC_CAST(_type_, _expr_)           __C_STYLE_CAST(_type_, _expr_)
#   define   __REINTERPRET_CAST(_type_, _expr_)      __C_STYLE_CAST(_type_, _expr_)
#   define   __DYNAMIC_CAST(_type_, _expr_)          __C_STYLE_CAST(_type_, _expr_)
#endif  /* def IXIN_WANT_NEW_STYLE_CASTS */
