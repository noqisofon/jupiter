﻿#pragma once

#include <jupiter/jupiter.hxx>

namespace jupiter {
    namespace component_model {


        interface ISynhronizeInvoke {
            JUPITER_BECOME_INTERFACE( ISynhronizeInvoke );

            virtual jupiter::Object* const invoke( jupiter::Delegate* const& method, jupiter::array<jupiter::Object*>* const& args ) __pure;
        };


    }
}
