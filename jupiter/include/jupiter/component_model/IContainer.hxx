﻿#pragma once

#include <jupiter/jupiter.hxx>

#include <jupiter/IDisposable.hxx>
#include <jupiter/String.hxx>
#include <jupiter/component_model/IComponent.hxx>
#include <jupiter/component_model/ISite.hxx>


namespace jupiter {
    namespace component_model {


        /*!
         *
         */
        interface IContainer : public jupiter::IDisposable {
            JUPITER_BECOME_INTERFACE( IContainer );

            virtual void add( IComponent* const& a_component ) __pure;
            virtual void add( IComponent* const& a_component, const jupiter::String* const& name ) __pure;

            virtual void remove( IComponent* const& a_component ) __pure;
        };


    }
}
