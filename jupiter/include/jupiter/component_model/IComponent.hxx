#pragma once

#include <jupiter/jupiter.hxx>

#include <jupiter/IDisposable.hxx>
#include <jupiter/component_model/ISite.hxx>

namespace jupiter {
    namespace component_model {


        /*!
         *
         */
        interface IComponent {
            JUPITER_BECOME_INTERFACE ( IComponent );

            virtual ISite* const getSite() const __pure;
            virtual void         setSite ( ISite* const& value ) __pure;
        };


    }
}
