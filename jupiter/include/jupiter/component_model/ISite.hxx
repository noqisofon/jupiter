﻿#pragma once

#include <jupiter/jupiter.hxx>

namespace jupiter {
    namespace component_model {


        interface ISite {
            JUPITER_BECOME_INTERFACE ( ISite );
        };


    }
}
